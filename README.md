# Slit Decision

[slitdecision.com](https://slitdecision.com).

A website that, when you enter two possible options for you to do, specifies
which one you should do. The choice is made based on a Quantum Random Number
Generator (from [qrng.anu.edu.au](https://qrng.anu.edu.au/)).

This means that, according to the [Many Worlds
Interpretation](https://en.wikipedia.org/wiki/Many-worlds_interpretation),
there will be a universe in which you will do option 1, and a universe in which
you will do option 2, since you base your decision on a quantum measurement.
