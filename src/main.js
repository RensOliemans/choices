if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./sw.js');
}

const setResult = (s) => {
    document.getElementById("result").innerText = s;
};

const setOptionOne = (b) => {
    const one = document.getElementById("one");
    const two = document.getElementById("two");
    if (b == null) {
        one.classList.remove("chosen", "nonchosen");
        two.classList.remove("chosen", "nonchosen");
        return;
    }

    if (b) {
        one.classList.add("chosen");
        one.classList.remove("nonchosen");
        two.classList.add("nonchosen");
        two.classList.remove("chosen");
    } else {
        one.classList.remove("chosen");
        one.classList.add("nonchosen");
        two.classList.remove("nonchosen");
        two.classList.add("chosen");
    }
};

const handleSubmit = () => {
    const one = document.getElementById("one");
    const two = document.getElementById("two");
    choose(one, two);
    return false;
}


const choose = (one, two) => {
    if (one && two) {
        getChoice().then(x => {
            handleChoice(one, two, x);
        });
        setResult("Getting quantom random numbers...");
    } else {
        setResult("Enter something in both fields");
    }
};

const handleChoice = (one, two, x) => {
    setResult(createOutput(one, two, x));
    setOptionOne(x);
};

const createOutput = (one, two, x) => {
    return `You should do "${x ? one.value : two.value}"`
};

const handleChange = () => {
    setOptionOne(null);
};



const getRandomUint8 = () => {
    return new Promise((resolve, reject) => {
        const url = new URL("https://qrng.anu.edu.au/API/jsonI.php");
        url.searchParams.append("length", "1");
        url.searchParams.append("type", "uint8");

        const request = new XMLHttpRequest();
        request.responseType = "json";

        request.onreadystatechange = () => {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    reject(Error(request.status.toString()));
                }
            }
        };
        request.onerror = () => {
            reject(Error("Network Error"));
        };

        request.open('GET', url.toString(), true);
        request.send();
    });
};
  
  
const getChoice = () => {
    return getRandomUint8()
        .then(result => result.data[0])
        .then(uint8 => uint8 % 2 === 0);
}